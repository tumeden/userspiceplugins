<?php if(count(get_included_files()) ==1) die(); //Direct Access Not Permitted
if(!pluginActive('forum',true)){die;}
if(!forumAccess($board,"read",$uid)){
  Redirect::to($currentPage."?err=Board+not+available");
}

$b = $db->query("SELECT * FROM forum_boards WHERE id = ? AND disabled = 0",[$board])->first();
$threadsQ = $db->query("SELECT * FROM forum_threads WHERE board = ? AND deleted = 0 ORDER BY last DESC",[$board]);
$threadsC = $threadsQ->count();
$threads = $threadsQ->results();
?>


<!-- Buttons --> 
<div class="shadow-sm p-3 mb-5 bg-body rounded">
  <div class="flex-container">
    <div class="row">
      <div class="col-auto">
          <button type="button" onclick="window.location.href = '<?=$currentPage?>';" name="button" class="btn btn-secondary">Return to Categories</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-bs-toggle="modal" data-target="#newTopic" data-bs-target="#newTopic">Post New Topic</button>
      </div>
    </div>
  </div>
</div>


<!-- New Topic Modal  -->
<div class="modal fade" id="newTopic" tabindex="-1" aria-labelledby="newtopicModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
      <button type="button" class="btn-close" data-dismiss="modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <?php include 'new_post.php';?>
      </div>
    </div>
  </div>
</div>

<!-- New Topic Modal End --> 



<!-- Page Style --> 
<h3 class="text-center"><?=$b->board?></h3>
<div class="table-responsive">
<table class="table table-striped table-hover table-borderless">
    <thead class="thead-dark">
      <th>Subject</th><th>Started By</th><th>Posts</th><th>Last Post</th>
    </thead>
    <tbody>
    <?php foreach($threads as $t){?>
      <tr>
        <td><a href="<?=$currentPage?>?board=<?=$board?>&thread=<?=$t->id?>">
		
		<div class="alert alert-dark" role="alert">
		<h4><?=$t->title?></a></h4>
</div></td>
        <td><?php echouser($t->created_by);?></td>
        <td><?=$db->query("SELECT id FROM forum_messages WHERE thread = ? AND disabled = 0",[$t->id])->count();?></td>
        <td><?=$t->last?></td>
      </tr>
      <?php } ?>
    </tbody>
</table>



